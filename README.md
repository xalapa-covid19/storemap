Storemap
========

Un mapa de tiendas en xalapa

<a href="https://github.com/vchaptsev/cookiecutter-django-vue">
    <img src="https://img.shields.io/badge/built%20with-Cookiecutter%20Django%20Vue-blue.svg" />
</a>


## Desarrollo

Instala [Docker](https://docs.docker.com/install/) y [Docker-Compose](https://docs.docker.com/compose/). Luego puedes iniciar los servicios con:

`docker-compose up --build`

Y si todo sale bien deberías poder crear un superusuario con:

`docker-compose run backend python manage.py createsuperuser`

## Sin docker

Si, en pleno 2020 habemos los que nos resistimos fieramente. O para quienes no funciona. En ese caso puedes:

### Backend

Tener instaladas las dependencias:

* gdal < 3
* postgresql + postgis
* pyhton 3
* node 10

Crear un entorno virtual usando pipenv o virtualenv y desde ahí usar:

`./manage.py migrate`

para correr las migraciones,

`./manage.py createsuperuser`

para crear un usuario,

`./manage.py makemigrations`

para crear nuevas migraciones luego de modificar los modelos. Y finalmente

`./manage.py runserver`

para correr el servidor, que podrás ver en http://localhost:8000

#### Tests

Para correr los tests hay que ejecutar el comando:

```bash
docker-compose -f docker-compose.yml run backend pytest --ds=config.settings
```

Si queremos un resumen con la cobertura de los tests:
```bash
docker-compose -f docker-compose.yml run backend pytest --cov backend --ds=config.settings
```



### Frontend

Lo usual debería funcionar:

`npm install`

para instalar todo, y

`npm run serve`

para correr el proyecto. Lo podrás ver en http://localhost:8080
