import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,

  state: {
    tags: [],
  },

  getters: {
    tagifyTags(state) {
      return state.tags.map((tag) => ({
        code: tag.id.toString(),
        value: tag.name,
      }));
    },
  },

  mutations: {
    setTags(state, data) {
      state.tags = data;
    },
    addTag(state, data) {
      state.tags.push(data);
    },
  },

  actions: {
    loadTags({ commit }) {
      fetch('/api/label/')
        .then((req) => req.json())
        .then((data) => commit('setTags', data));
    },
    createTag({ commit }, nameLabel) {
      const data = { name: nameLabel, description: 'Created for user' };
      return fetch('/api/label/', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((response) => {
          commit('addTag', response);
          return response;
        });
    },
  },
});

export default store;
